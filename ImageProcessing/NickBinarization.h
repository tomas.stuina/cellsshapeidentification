#pragma once

class NickBinarization
{
private:
	Mat & m_grayscaleImage;
	int m_width = 0;
	int m_height = 0;
	int m_blockSize = 0;
	double m_k = 0.0;

public:

	NickBinarization(Mat& grayscaleImage, int blockSize, double k);
	~NickBinarization();

	void ApplyBinarization(Mat& outImage);

};


