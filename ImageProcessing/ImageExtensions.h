#pragma once

struct RGB {
	uchar blue;
	uchar green;
	uchar red;
};

class ImageExtensions
{
public:
	static void Dilate(Mat& image, Mat& imageOut);
	static double CalculateDistance(Point i, Point j);
    static bool IsCountourOnSide(vector<Point> points, int width, int height, int offset);
	static double GetLargestQuadrilateralArea(vector<Point> points, vector<Point>& quadrilateralPoints);
	static double GetLargestTriangleLine(vector<Point> trianglePoints, vector<Point>& linePoints);
	static bool AreAllSidesEqual(vector<Point> points, double err);
	static double GetBoundingBoxRatio(vector<Point> points);
	static void GetBoundingBox(vector<Point> points, vector<Point>& boundingPoints);
	static double GetLargestTriangleArea(vector<Point> points, vector<Point>& trianglePoints);
	static void RGBToGrayscale(Mat const& rgbImage, Mat& grayScaleImage, Mat* histogram);
};
