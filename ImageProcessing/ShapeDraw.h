#pragma once
class ShapeDraw
{
private:
	int m_lineThickness = 2;
	Mat& m_image;
public:
	ShapeDraw(Mat& image);
	~ShapeDraw();

	void DrawEllipse (RotatedRect ellipseRect, Scalar color);
	void DrawContours(vector<Point> points, Scalar color);
	void DrawPolygon(vector<Point> points, Scalar color);
};

