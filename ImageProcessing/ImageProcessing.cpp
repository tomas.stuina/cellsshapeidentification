#include "stdafx.h"
#include "OtsuBinarization.h"
#include "NickBinarization.h"
#include "ImageExtensions.h"
#include "PolylinesApprox.h"
#include "MathExtensions.h"
#include "ShapeDetector.h"
#include "ShapeDraw.h"


int main(int argc, char** argv)
{
	String imageSource, imageDestination;
	bool isNICK = true;

	if (argc < 7) {
		cerr << "Naudojimas: " << argv[0] << " -s image.png -d image.bmp -b nick|otsu" << std::endl;
		return 1;
	}

	for (int i = 1; i < argc; ++i) {
		std::string arg = argv[i];
		if (arg == "-s" || arg == "--source")
		{
			imageSource = argv[i + 1];
		}
		else if (arg == "-d" || arg == "--destination")
		{
			imageDestination = argv[i + 1];
		}
		else if (arg == "-b" || arg == "--binarization")
		{
			isNICK = argv[i + 1] != "otsu";
		}
		else
			continue;
	}

	Mat image, bgrImage, grayscaleImage, calculatedHistogram;
	bgrImage = imread(imageSource, IMREAD_COLOR);
	cvtColor(bgrImage, image, COLOR_BGR2RGB);

	if (image.empty())
	{
		cerr << "Negalima atidaryti vaizdo!" << endl;
		return 1;
	}

	int width = image.cols;
	int height = image.rows;

	grayscaleImage = Mat::zeros(Size(image.cols, image.rows), CV_8UC1);
	Mat resultImage = Mat::zeros(Size(image.cols, image.rows), CV_8UC1);
	calculatedHistogram = Mat::zeros(Size(1, 256), CV_8UC1);

	ImageExtensions::RGBToGrayscale(image, grayscaleImage, &calculatedHistogram);
	grayscaleImage.copyTo(resultImage);

	Mat input = Mat::zeros(Size(image.cols, image.rows), CV_8UC1);
	grayscaleImage.copyTo(input);

	if (isNICK)
	{
		NickBinarization nick(input, 45, 0.2);
		nick.ApplyBinarization(grayscaleImage);
	}
	else
	{
		OtsuBinarization otsu(input, calculatedHistogram);
		otsu.ApplyBinarization(grayscaleImage);
	}

	Mat cannyOutput;
	Canny(grayscaleImage, cannyOutput, 100, 255);

	Mat opening;
	cannyOutput.copyTo(opening);
	ImageExtensions::Dilate(opening, cannyOutput);

	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;
	findContours(cannyOutput, contours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_NONE, Point(0, 0));

	vector< vector<Point> > hull(contours.size());

	Mat hullImg = Mat::zeros(cannyOutput.size(), CV_8UC3);
	image.copyTo(hullImg);

	for (int i = 0; i < contours.size(); i++)
	{

		convexHull(Mat(contours[i]), hull[i], false);
		hull[i] = PolylinesApprox::Approximate(hull[i], 0.15);

		double hullArea = contourArea(hull[i]);

		if (ImageExtensions::IsCountourOnSide(hull[i], width, height, 1))
			continue;

		Scalar shapeColor;
		ShapeDetector shapeDetector;
		Shape detectedShape = shapeDetector.Detect(hull[i]);
		switch (detectedShape)
		{
		case Shape::Diamond:
			shapeColor = Scalar(153, 255, 51);
			break;
		case Shape::Circle:
			shapeColor = Scalar(0, 0, 255);
			break;
		case Shape::Ellipse:
			shapeColor = Scalar(255, 254, 51);
			break;
		case Shape::Polygon:
			shapeColor = Scalar(2, 255, 2);
			break;
		case Shape::Rectangle:
			shapeColor = Scalar(255, 20, 147);
			break;
		case Shape::Square:
			shapeColor = Scalar(128, 90, 70);
			break;
		case Shape::Triangle:
			shapeColor = Scalar(230, 0, 2);
			break;
		case Shape::ParallelogramOrTrapezoid:
			shapeColor = Scalar(0, 191, 255);
			break;
		case Shape::Unknown:
			break;
		default:
			break;
		}

		ShapeDraw shapeDraw(hullImg);
		switch (detectedShape)
		{
		case Shape::Diamond:
		case Shape::Rectangle:
		case Shape::Square:
		case Shape::ParallelogramOrTrapezoid:
			shapeDraw.DrawContours(shapeDetector.GetEnclosedQuadrilateral(), shapeColor);
			break;

		case Shape::Triangle:
			shapeDraw.DrawContours(shapeDetector.GetEnclosedTriangle(), shapeColor);
			break;

		case Shape::Ellipse:
		case Shape::Circle:
			shapeDraw.DrawEllipse(shapeDetector.GetBoundedEllipse(), shapeColor);
			break;

		case Shape::Polygon:
			shapeDraw.DrawPolygon(hull[i], shapeColor);
			break;
		}
	}

	double alpha = 0.2;
	Mat resultmage;
	addWeighted(hullImg, alpha, image, 1.0 - alpha, 0.0, resultmage);
	cvtColor(resultmage, resultmage, COLOR_BGR2RGB);
	imwrite(imageDestination, resultmage);

    return 0;
}
