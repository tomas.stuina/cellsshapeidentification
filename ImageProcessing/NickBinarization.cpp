#include "stdafx.h"
#include "NickBinarization.h"


NickBinarization::NickBinarization(Mat& grayscaleImage, int blockSize, double k)
: m_grayscaleImage(grayscaleImage), m_width(grayscaleImage.cols), m_height(grayscaleImage.rows), m_blockSize(blockSize), m_k(k)
{
}


NickBinarization::~NickBinarization()
{
}

void NickBinarization::ApplyBinarization(Mat & outImage)
{
	Mat thresh;
	Mat mean, sqmean, variance, sqrtVarianceMeanSum;
	double srcMin, stddevMax;
	boxFilter(m_grayscaleImage, mean, CV_32F, Size(m_blockSize, m_blockSize), Point(-1, -1), true, BORDER_REPLICATE);
	sqrBoxFilter(m_grayscaleImage, sqmean, CV_32F, Size(m_blockSize, m_blockSize), Point(-1, -1), true, BORDER_REPLICATE);
	variance = sqmean - mean.mul(mean);
	sqrt(variance + sqmean, sqrtVarianceMeanSum);
	thresh = mean + static_cast<float>(m_k) * sqrtVarianceMeanSum;
	thresh.convertTo(thresh, m_grayscaleImage.depth());

	Mat threshMask;
	compare(m_grayscaleImage, thresh, threshMask, CMP_GT);
	outImage.setTo(0);
	outImage.setTo(255, threshMask);
}
