#include "stdafx.h"
#include "MathExtensions.h"

bool MathExtensions::AreDoublesEqual(double valueA, double valueB)
{
	return fabs(valueA - valueB) <= numeric_limits<double>::epsilon() * fabs(valueA);
}

bool MathExtensions::IsDoubleGreaterOrEqual(double valueA, double valueB)
{
	return valueA > valueB || AreDoublesEqual (valueA, valueB);
}

bool MathExtensions::IsDoubleLessOrEqual(double valueA, double valueB)
{
	return valueA < valueB || AreDoublesEqual(valueA, valueB);
}

double MathExtensions::GetQuadrilateralArea (Point a, Point b, Point c, Point d)
{
	vector<Point> points;

	points.push_back(a);
	points.push_back(b);
	points.push_back(c);
	points.push_back(d);

	return contourArea(points);
}

double MathExtensions::GetTriangleArea(Point a, Point b, Point c)
{
	return 0.5 * abs(a.x * b.y + b.x * c.y + c.x * a.y
		- a.y * b.x - b.y * c.x - c.y * a.x);
}

double MathExtensions::GetTriangleArea(int x0, int y0, int x1, int y1, int x2, int y2)
{
	return GetTriangleArea(Point(x0, y0), Point(x1, y1), Point(x2, y2));
}

double MathExtensions::GetEllipiseLength(double a, double b)
{
	double t = pow ((a-b)/(a+b), 2.0);

    return CV_PI * (a + b) * (1.0 + ((3.0 * t) / (10.0 + sqrt(4.0 - 3.0 * t))));
}

double MathExtensions::GetEllipiseArea(double a, double b)
{
    return CV_PI * a * b;
}