
#pragma once

#include "targetver.h"
#define _USE_MATH_DEFINES

#include <stdio.h>
#include <tchar.h>
#include <atlimage.h>
#include <opencv2/core.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include "opencv2/imgproc.hpp"
#include <fstream>
#include <iostream>
#include <string>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <deque>

#include <filesystem>

using namespace cv;
using namespace std;
