#include "stdafx.h"
#include "ShapeDetector.h"
#include "ImageExtensions.h"
#include "MathExtensions.h"


bool ShapeDetector::IsPossibleTriangle()
{
	return fabs(m_triangleAreaRatio - m_triangleLengthRatio) < 0.2;
}

bool ShapeDetector::IsPossibleCircle()
{
	return (4 * CV_PI) <= m_circleRatio && m_circleRatio <= 13.5;
}

bool ShapeDetector::IsPossibleRectangle()
{
	return fabs(m_rectangleRatio) < 0.1 && m_diamondRatio > 0.72;
}

bool ShapeDetector::IsPossibleDiamond()
{
	return 0.52 <= m_diamondRatio && m_diamondRatio <= 0.78;
}

bool ShapeDetector::IsPossibleElipse()
{
	return fabs(m_ellipseRatio - 0.7) <= 0.2 && m_circleRatio > 13.2;
}

void ShapeDetector::IntializeHullInformation(vector<Point> hull)
{
	m_hullLength =  arcLength(hull, true);
	m_hullArea = contourArea(hull);

	m_enclosedQuadrilateralArea = ImageExtensions::GetLargestQuadrilateralArea(hull, m_enclosedQuadrilateral);
	m_enclosedTriangleArea = ImageExtensions::GetLargestTriangleArea(hull, m_enclosedTriangle);
	m_enclosedTriangleLength = arcLength (m_enclosedTriangle, true);

	ImageExtensions::GetBoundingBox(hull, m_boundingRectangle);
	
	m_boundingRectangleArea = contourArea(m_boundingRectangle);
	m_boundingRectangleLength = arcLength(m_boundingRectangle, true);

	m_triangleAreaRatio = m_enclosedTriangleArea / m_hullArea;
	m_triangleLengthRatio = m_enclosedTriangleLength / m_hullLength;
	m_ellipseRatio = m_enclosedQuadrilateralArea / m_hullArea;
	m_rectangleRatio = fabs((m_hullArea / m_boundingRectangleArea) - (m_hullArea / m_enclosedQuadrilateralArea));
	m_diamondRatio = m_enclosedTriangleArea / m_hullArea;
	m_circleRatio = pow(m_hullLength, 2.0) / m_hullArea;

	if (hull.size() >= 5)
	{
	m_boundedEllipse = fitEllipse(hull);
	double a = m_boundedEllipse.size.height / 2.0;
	double b = m_boundedEllipse.size.width / 2.0;

	double ellipseLength = MathExtensions::GetEllipiseLength(a, b);
	double ellipseArea = MathExtensions::GetEllipiseArea(a, b);
	m_ellipseDiagonalRatio = fabs(1 - a / b);
	m_ellipseLengthRatio = fabs(1 - (m_hullLength / ellipseLength));
	m_ellipseAreaRatio = fabs(1 - (m_hullArea / ellipseArea));
	}
}

ShapeDetector::ShapeDetector()
{
}


ShapeDetector::~ShapeDetector()
{
}

Shape ShapeDetector::Detect(vector<Point> hull)
{
	if (IsLine (hull))
		return Shape::Unknown;

	IntializeHullInformation(hull);

	if (IsAreaFromNoise (hull))
		return Shape::Unknown;

	bool isDiamond = IsPossibleDiamond ();
	bool isRectangle = IsPossibleRectangle ();
	bool isCircle = IsPossibleCircle ();
	bool isElipse = IsPossibleElipse ();
	bool isTriangle = IsPossibleTriangle ();

	if (isDiamond && !isElipse && !isRectangle && !isCircle && !isTriangle)
	{
		if (ImageExtensions::AreAllSidesEqual(m_enclosedQuadrilateral, 0.2))
			return Shape::Diamond;

		return Shape::ParallelogramOrTrapezoid;
	}
	else if (isElipse && !isRectangle && !isCircle && !isTriangle)
	{
		if (hull.size() >= 5)
		{
			if (m_ellipseLengthRatio < 0.15 && m_ellipseAreaRatio < 0.15 && m_ellipseDiagonalRatio >= 0.1)
			{
				return Shape::Ellipse;
			}
			else
			{
				return Shape::Polygon;
			}
		}
	}
	else if (isCircle)
	{
		return Shape::Circle;
	}
	else if (isRectangle)
	{
		if (ImageExtensions::AreAllSidesEqual(m_boundingRectangle, 0.2))
			return Shape::Square;

		return Shape::Rectangle;
	}
	else if (isTriangle)
	{
		return Shape::Triangle;
	}

	return Shape::Unknown;
}

bool ShapeDetector::IsLine (vector<Point> points) const
{
	if (points.size () < 3)
		return true;

	return ImageExtensions::GetBoundingBoxRatio(points) < 0.3;
}

bool ShapeDetector::IsAreaFromNoise(vector<Point> points) const
{
	return m_hullArea <= 100;
}

vector<Point> ShapeDetector::GetEnclosedTriangle() const
{
	return m_enclosedTriangle;
}

vector<Point> ShapeDetector::GetEnclosedQuadrilateral() const
{
	return m_enclosedQuadrilateral;
}

RotatedRect ShapeDetector::GetBoundedEllipse() const
{
	return m_boundedEllipse;
}
