#include "stdafx.h"
#include "ShapeDraw.h"


ShapeDraw::ShapeDraw(Mat& image)
: m_image(image)
{
}


ShapeDraw::~ShapeDraw()
{
}

void ShapeDraw::DrawEllipse(RotatedRect ellipseRect, Scalar color)
{
	ellipse(m_image, ellipseRect, color, m_lineThickness, LINE_8);
}

void ShapeDraw::DrawContours(vector<Point> points, Scalar color)
{
	drawContours(m_image, vector<vector<Point>> {points}, 0, color, m_lineThickness, LINE_8);
}

void ShapeDraw::DrawPolygon(vector<Point> points, Scalar color)
{
	Rect rect = boundingRect(points);
	int textX = (rect.x + (rect.width / 2)) - 2;
	int textY = (rect.y + (rect.height / 2)) - 2;
	drawContours(m_image, vector<vector<Point>> {points}, 0, color, m_lineThickness, LINE_8);
	putText(m_image, to_string(points.size()), Point(textX, textY), FONT_HERSHEY_SIMPLEX, 0.4, Scalar(255, 255, 255), 1, LINE_8);
}
