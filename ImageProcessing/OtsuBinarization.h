#pragma once
class OtsuBinarization
{
private:
	Mat& m_grayscaleImage;
	int m_width = 0;
	int m_height = 0;
	Mat const& m_histogram;

	int CalculateOtsuThreshold(Mat const& histogram);
	void ApplyThreshold(Mat& grayscaleImage, int threshold);
public:
	OtsuBinarization(Mat& grayscaleImage, Mat const& histogram);
	~OtsuBinarization();

	void ApplyBinarization(Mat& outImage);

};

