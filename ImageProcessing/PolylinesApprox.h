#pragma once
class PolylinesApprox
{
public:
    PolylinesApprox();
    ~PolylinesApprox();

static double CalculateDistance(vector<Point> point);

static double CalculateDistance(Point i, Point j);

static double CalculateMaxDeviation(vector<Point> points);

static double CalculateMaxDeviationForSegment(vector<Point> points, int i, int j);

static double CalculateDistance(Point iP, Point jP, Point kP);

static double CalculateDistanceEx(Point iP, Point jP, Point kP);

static vector<Point> FilterOutCollinearPoints(vector<Point> contour, double dt, int& pointsDeleted);

    static vector<Point> Approximate (vector<Point> contour, double rT);
};

