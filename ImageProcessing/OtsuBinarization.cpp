#include "stdafx.h"
#include "OtsuBinarization.h"


OtsuBinarization::OtsuBinarization(Mat& grayscaleImage, Mat const& histogram) : m_grayscaleImage(grayscaleImage),
m_width(grayscaleImage.cols), m_height(grayscaleImage.rows), m_histogram(histogram)
{
}


OtsuBinarization::~OtsuBinarization()
{
}


int OtsuBinarization::CalculateOtsuThreshold(Mat const& histogram)
{
	int totalValuesSum = 0;
	int total = 0;

	for (int i = 0; i < histogram.rows; i++)
	{
		int level = static_cast<int>(histogram.ptr<uchar>(i)[0]);
		totalValuesSum += i * level;
		total += level;
	}

	float currentValuesSum = 0;
	int weight0 = 0;
	int weight1 = 0;

	float maximumVariance = 0;
	int thresholdValue = 0;

	for (int i = 0; i < histogram.rows; i++)
	{
		int level = static_cast<int>(histogram.ptr<uchar>(i)[0]);
		weight0 += level;
		if (weight0 == 0)
			continue;

		weight1 = total - weight0;
		if (weight1 == 0)
			break;

		currentValuesSum += (i * level);

		float mean0 = currentValuesSum / weight0;
		float mean1 = totalValuesSum - currentValuesSum / weight1;
		float varianceBetween = (weight0 * weight1) * (mean1 - mean0) * (mean1 - mean0);

		if (varianceBetween > maximumVariance)
		{
			maximumVariance = varianceBetween;
			thresholdValue = i;
		}
	}

	return thresholdValue;
}

void OtsuBinarization::ApplyThreshold (Mat& outImage, int threshold)
{
	Mat thresh = Mat::ones(Size(m_width, m_height), CV_8UC1) * threshold;
	Mat threshMask;
	compare(m_grayscaleImage, thresh, threshMask, CMP_GT);
	outImage.setTo(0);
	outImage.setTo(255, threshMask);
}

void OtsuBinarization::ApplyBinarization (Mat& outImage)
{
	ApplyThreshold(outImage, CalculateOtsuThreshold(m_histogram));
}