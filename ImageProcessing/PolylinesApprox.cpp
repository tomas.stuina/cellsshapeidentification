#include "stdafx.h"
#include "PolylinesApprox.h"
#include "MathExtensions.h"


PolylinesApprox::PolylinesApprox()
{
}


PolylinesApprox::~PolylinesApprox()
{
}

double PolylinesApprox::CalculateDistance (vector<Point> points)
{ 
    int size = points.size();
    double totalDistance = 0.0;
    for (int i = 0; i < size - 1; i++)
    {
    totalDistance += CalculateDistance (points[i], points[i + 1]);
    }

	totalDistance += CalculateDistance(points[0], points[size - 1]);

    return totalDistance;
}

double PolylinesApprox::CalculateDistance (Point i, Point j)
{ 
  int dx  = i.x - j.x;         
  int dy  = i.y - j.y;         
  return sqrt( pow(dx, 2.0) + pow(dy, 2.0) );
}


double PolylinesApprox::CalculateMaxDeviation(vector<Point> points) {
	double cdev = 0.0;
	double maxdev = 0.0;

	int size = points.size();

	for (int i = 0; i < size; i++)
	{
		cdev = CalculateMaxDeviationForSegment(points, i, (i + 1) % size);

		if (cdev > maxdev) {
			maxdev = cdev;
		}
	}

	return maxdev;
}

double PolylinesApprox::CalculateMaxDeviationForSegment(vector<Point> points, int i, int j)
{
	double maxDeviation = 0.0;
	int size = points.size();

	if (points[i] != points[j])
	{
		int n = (i + 1) % size;
		while (n != i)
		{

			double distance = CalculateDistanceEx(points[i], points[n], points[j]);
			if (distance > maxDeviation) {
				maxDeviation = distance;
			}
			n = (n + 1) % size;
		}
	}

	return maxDeviation;
}

double PolylinesApprox::CalculateDistance(Point iP, Point jP, Point kP)
    {
    double numerator = pow(((jP.x - iP.x)*(kP.y - iP.y)) - ((jP.y - iP.y) * (kP.x - iP.x)), 2.0);
	double denominator = pow(iP.x - kP.x, 2.0) + pow(iP.y - kP.y, 2.0);
	return fabs (sqrt(numerator / denominator));
    }

double PolylinesApprox::CalculateDistanceEx(Point iP, Point jP, Point kP)
{
	double numerator = fabs((jP.x * (iP.y - kP.y)) + (jP.y * (kP.x - iP.x)) + (kP.y*iP.x) - (iP.y * kP.x));
	double denominator = sqrt(pow(kP.x - iP.x, 2.0) + pow(iP.y - kP.y, 2.0));
	return fabs(numerator / denominator);
}

vector<Point> PolylinesApprox::FilterOutCollinearPoints(vector<Point> contour, double dt, int& pointsDeleted)
{
	int size = contour.size();
	if (size < 3)
		return contour;

	bool* lookUpTable = new bool[size];
	for (int i = 0; i < size; i++)
		lookUpTable[i] = false;

	vector<Point> approx;
	Point start(contour[0]);

	int iI = 0;
	int jI = 1;
	int kI = 2;

	Point iP = start;
	Point jP = contour[0 + 1];
	Point kP = contour[0 + 2];

	do
	{
		double collinearDistance = CalculateDistance (iP, jP, kP);
		if (collinearDistance <= dt) // remove colinear point
		{
			lookUpTable[jI] = true;
			jP = kP;
			pointsDeleted++;
		}
		else
		{
			iP = jP;
			jP = kP;
		}

		jI++;
		kI++;
		if (kI >= size)
			kI = 0;

		kP = contour[kI];

	} while (jP != start);


	for (int i = 0; i < size; i++)
	{
		if (lookUpTable[i])
			continue;

		approx.push_back(contour[i]);
	}

	delete lookUpTable;
	return approx;
}

vector<Point> PolylinesApprox::Approximate(vector<Point> contour, double rT) // rt between 0,5 and 0,7
{
	double dt = 0.1;
    double r = 0.0;

	int maximumPointsDeleted = 0;

	double maximumMaxErr = 0.0;
	double initialDistance = arcLength (contour, false);
	double maxDelta = 0.0;

	do
	{
		int currentPointsDeleted = 0;

		contour = FilterOutCollinearPoints(contour, dt,  currentPointsDeleted);
		
        double maxErr = CalculateMaxDeviation (contour);

        double currentDistance = arcLength (contour, false);
		double deltaDistance = initialDistance - currentDistance;
        initialDistance = currentDistance;

		maxDelta = max(maxDelta, deltaDistance);

		maximumMaxErr = max(maximumMaxErr, maxErr);

        if (MathExtensions::AreDoublesEqual (maxErr, 0.0))
            return contour;

        r = maxDelta / maxErr;
        dt+= 0.5;

        if (r >= rT)
            return contour;
	} while (true);

	return contour;
}
