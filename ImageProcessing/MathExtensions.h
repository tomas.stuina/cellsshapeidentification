#pragma once
class MathExtensions
{
public:
	static bool AreDoublesEqual(double valueA, double valueB);
	static bool IsDoubleGreaterOrEqual(double valueA, double valueB);
	static bool IsDoubleLessOrEqual(double valueA, double valueB);
	static double GetQuadrilateralArea(Point a, Point b, Point c, Point d);
	static double GetTriangleArea(Point a, Point b, Point c);
	static double GetTriangleArea(int x0, int y0, int x1, int y1, int x2, int y2);
    static double GetEllipiseLength(double a, double b);
    static double GetEllipiseArea(double a, double b);
};
