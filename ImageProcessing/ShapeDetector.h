#pragma once

enum class Shape
{
Unknown,
Diamond,
Circle,
Ellipse,
Polygon,
Rectangle,
Square,
Triangle,
ParallelogramOrTrapezoid
};

class ShapeDetector
{
private:
	vector<Point> m_enclosedTriangle;
	vector<Point> m_enclosedQuadrilateral;
	vector<Point> m_boundingRectangle;
	RotatedRect m_boundedEllipse;

	double m_hullLength = 0.0;
	double m_hullArea = 0.0;
	double m_enclosedTriangleArea = 0.0;
	double m_enclosedTriangleLength = 0.0;
	double m_enclosedQuadrilateralArea = 0.0;
	double m_boundingRectangleArea = 0.0;
	double m_boundingRectangleLength = 0.0;

	double m_triangleAreaRatio = 0.0;
	double m_triangleLengthRatio = 0.0;

	double m_ellipseRatio = 0.0;
	double m_ellipseLengthRatio = 0.0;
	double m_ellipseAreaRatio = 0.0;
	double m_ellipseDiagonalRatio = 0.0;

	double m_rectangleRatio = 0.0;
	double m_diamondRatio = 0.0;
	double m_circleRatio = 0.0;

	bool IsPossibleTriangle();
	bool IsPossibleCircle();
	bool IsPossibleRectangle();
	bool IsPossibleDiamond();
	bool IsPossibleElipse();

	void IntializeHullInformation(vector<Point> hull);

public:
	ShapeDetector();
	~ShapeDetector();

	Shape Detect(vector<Point> hull);

	bool IsLine(vector<Point> points) const;

	bool IsAreaFromNoise(vector<Point> points) const;

	vector<Point> GetEnclosedTriangle() const;
	vector<Point> GetEnclosedQuadrilateral() const;
	RotatedRect GetBoundedEllipse() const;
};

