#include "stdafx.h"
#include "ImageExtensions.h"
#include "MathExtensions.h"


void ImageExtensions::Dilate(Mat& image, Mat& imageOut)
{
	int size = 1;
	Mat element = getStructuringElement(MORPH_ELLIPSE,
		Size(2 * size + 1, 2 * size + 1),
		Point(size, size));

	dilate( image, imageOut, element, Point (-1, -1), 1);

}

double ImageExtensions::CalculateDistance(Point i, Point j)
{
	int dx = i.x - j.x;
	int dy = i.y - j.y;
	return sqrt(pow(dx, 2.0) + pow(dy, 2.0));
}

bool ImageExtensions::IsCountourOnSide(vector<Point> points, int width, int height, int offset)
{
    int size = points.size ();
   if (size == 0)
    return false;

    for (int i = 0; i < size; i++)
    {
    Point first = points[i];
    if (first.x <= (0 + offset) || first.x >= ((width - 1) - offset) || first.y <= (0 + offset)  || first.y >= (height - 1) - offset)
        return true;
    }

return false;
}

double ImageExtensions::GetLargestQuadrilateralArea (vector<Point> points, vector<Point>& quadrilateralPoints)
{

	int size = points.size();
	double area = 0.0;

	if (size < 4)
		return area;

	int i = 0;
	int j = (i + 1) % size;
	int k = (j + 1) % size;
	int o = (k + 1) % size;

	Point a = points[i];
	Point b = points[j];
	Point c = points[k];
	Point d = points[o];

	area = MathExtensions::GetQuadrilateralArea(a, b, c, d);

	int x0 = a.x;
	int y0 = a.y;
	int x1 = b.x;
	int y1 = b.y;
	int x2 = c.x;
	int y2 = c.y;
	int x3 = d.x;
	int y3 = d.y;

	while (true)
	{

		while (MathExtensions::GetQuadrilateralArea(a, b, c, d) <= MathExtensions::GetQuadrilateralArea(a, b, c, points[(o + 1) % size]))
		{
			o = (o + 1) % size;
			d = points[o];
			while (MathExtensions::GetQuadrilateralArea(a, b, c, d) <= MathExtensions::GetQuadrilateralArea(a, b, points[(k + 1) % size], d))
			{
				k = (k + 1) % size;
				c = points[k];

			}
			while (MathExtensions::GetQuadrilateralArea(a, b, c, d) <= MathExtensions::GetQuadrilateralArea(a, points[(j + 1) % size], c, d ))
			{
				j = (j + 1) % size;
				b = points[j];

			}
		}

		double currentArea = MathExtensions::GetQuadrilateralArea(a, b, c, d);
		if (currentArea > area)
		{
			area = currentArea;
			x0 = a.x;
			y0 = a.y;
			x1 = b.x;
			y1 = b.y;
			x2 = c.x;
			y2 = c.y;
			x3 = d.x;
			y3 = d.y;
		}

		i = (i + 1) % size;
		a = points[i];


		if (i == 0)
			break;

		if (i == j)
		{
			j = (j + 1) % size;
			b = points[j];


			if (j == k)
			{
				k = (k + 1) % size;
				c = points[k];


				if (k == o)
				{
					o = (o + 1) % size;
					d = points[o];
				}
			}
		}
	}

	quadrilateralPoints.push_back(Point(x0, y0));
	quadrilateralPoints.push_back(Point(x1, y1));
	quadrilateralPoints.push_back(Point(x2, y2));
	quadrilateralPoints.push_back(Point(x3, y3));

	return area;
}

double ImageExtensions::GetLargestTriangleLine(vector<Point> trianglePoints, vector<Point>& linePoints)
{
	if (trianglePoints.size() < 3)
		return 0.0;

	Point a = trianglePoints[0];
	Point b = trianglePoints[1];
	Point c = trianglePoints[2];

	double ab = ImageExtensions::CalculateDistance(a, b);
	double bc = ImageExtensions::CalculateDistance(b, c);
	double ac = ImageExtensions::CalculateDistance(a, c);

	if (ab >= bc && ab >= ac)
	{
		linePoints.push_back(a);
		linePoints.push_back(b);
		return ab;
	}

	if (bc >= ab && bc >= ac)
	{
		linePoints.push_back(b);
		linePoints.push_back(c);
		return bc;
	}

	if (ac >= bc && ac >= ab)
	{
		linePoints.push_back(a);
		linePoints.push_back(c);
		return ac;
	}
}

bool ImageExtensions::AreAllSidesEqual(vector<Point> points, double err)
{
	if (points.size() < 4)
		return false;

	double a = ImageExtensions::CalculateDistance(points[0], points[1]);
	double b = ImageExtensions::CalculateDistance(points[1], points[2]);
	double c = ImageExtensions::CalculateDistance(points[2], points[3]);
	double d = ImageExtensions::CalculateDistance(points[3], points[0]);

	double ratioAB = fabs (1 - (a / b));


	bool matches = ratioAB < err;

	return matches;
}

double ImageExtensions::GetBoundingBoxRatio (vector<Point> points)
{
	RotatedRect rect = minAreaRect(points);
	if (rect.size.height < rect.size.width)
		return rect.size.height / rect.size.width;

	return rect.size.width / rect.size.height;
}

void ImageExtensions::GetBoundingBox(vector<Point> points, vector<Point>& boundingPoints)
{
	Rect rect = boundingRect(points);

	boundingPoints.push_back(Point(rect.x, rect.y));
	boundingPoints.push_back(Point(rect.x + rect.width, rect.y));
	boundingPoints.push_back(Point(rect.x + rect.width, rect.y + rect.height));
	boundingPoints.push_back(Point(rect.x, rect.y + rect.height));

}

double ImageExtensions::GetLargestTriangleArea(vector<Point> points, vector<Point>& trianglePoints)
{
	int size = points.size();
	double area = 0.0;

	if (size < 3)
		return area;

	int i = 0;
	int j = 1;
	int k = 2;

	Point a = points[i];
	Point b = points[j];
	Point c = points[k];

	area = MathExtensions::GetTriangleArea(a, b, c);

	int x0 = a.x;
	int y0 = a.y;
	int x1 = b.x;
	int y1 = b.y;
	int x2 = c.x;
	int y2 = c.y;

	while (true)
	{
		while (true)
		{
			while (MathExtensions::GetTriangleArea(a, b, c) <= MathExtensions::GetTriangleArea(a, b, points[(k + 1) % size]))
			{
				k = (k + 1) % size;
			}
			c = points[k];

			if (MathExtensions::GetTriangleArea(a, b, c) <= MathExtensions::GetTriangleArea(a, points[(j + 1) % size], c))
			{
				b = points[(j + 1) % size];
				j = (j + 1) % size;
				continue;
			}
			else
				break;
		}

		double currArea = MathExtensions::GetTriangleArea(a, b, c);
		if (currArea > MathExtensions::GetTriangleArea(x0, y0, x1, y1, x2, y2))
		{
			x0 = a.x;
			y0 = a.y;
			x1 = b.x;
			y1 = b.y;
			x2 = c.x;
			y2 = c.y;
			area = currArea;
		}

		a = points[(i + 1) % size];
		i = (i + 1) % size;

		if (i == j)
		{
			b = points[(j + 1) % size];
			j = (j + 1) % size;
		}

		if (j == k)
		{
			c = points[(k + 1) % size];
			k = (k + 1) % size;
		}

		if (i == 0)
			break;

	}

	trianglePoints.push_back(Point(x0, y0));
	trianglePoints.push_back(Point(x1, y1));
	trianglePoints.push_back(Point(x2, y2));

	return area;
}

void ImageExtensions::RGBToGrayscale(Mat const & rgbImage, Mat & grayScaleImage, Mat * histogram)
{
	for (int y = 0; y < rgbImage.rows; y++)
	for (int x = 0; x < rgbImage.cols; x++)
	{
		RGB rgb = rgbImage.ptr<RGB>(y)[x];
		double grayValue = ((static_cast<double> (rgb.red) * 0.3)
			+ (static_cast<double> (rgb.green) * 0.59)
			+ (static_cast<double> (rgb.blue) * 0.11));
		grayScaleImage.ptr<uchar>(y)[x] = static_cast<uchar> (grayValue);

		if (histogram != nullptr)
			histogram->ptr<uchar>(static_cast<int>(grayValue))[0]++;
	}
}
